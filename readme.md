# Nodejs SocketIO Chat App
This app have ultimate room. These rooms have several person.
Suppose room name : Room1
so when a person register by this room name(Room1), he can see all other members chat history those are registered in this room(Room1)
Also send the the current location to other.

## Server Side Technology
1. Nodejs
2. Express Server
3. socket.io (To communicate Client side and server side utility)


## Client Side Technology
1. Mustache template engine
2. JQuery library
3. Moment (Time utility library)

## Online Access
 https://young-dusk-86152.herokuapp.com
